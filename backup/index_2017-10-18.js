import React from 'react';
import ReactDOM from 'react-dom';

class Square extends React.Component {
	render() {
		return (
			<rect
				height={this.props.size}
				width={this.props.size}
				x={this.props.xPos}
				y={this.props.yPos}
				fill={this.props.color}
			/>
		);
	}
}

let borders = {
	x: [0,9],
	y: [0,14]
}

let colorMap = {
	0: "gray",
	1: "green",
	2: "red",
}

let stones = [
	[{x:0,y:0},{x:1,y:0},{x:0,y:1},{x:1,y:1}],
	[{x:0,y:0},{x:1,y:0},{x:2,y:0},{x:3,y:0}],
	[{x:0,y:0},{x:1,y:0},{x:2,y:0},{x:2,y:1}],
	[{x:0,y:0},{x:1,y:0},{x:2,y:0},{x:0,y:1}],
	[{x:0,y:0},{x:1,y:0},{x:1,y:2},{x:2,y:2}],
	[{x:1,y:0},{x:2,y:0},{x:0,y:1},{x:1,y:1}]
]

function place(stone, position) {
	let result = [];
	for(let i=0;i<4;i++) {
		result.push({
			x:stone[i].x+position.x,
			y:stone[i].y+position.y
		});
	}
	return result;
}

function step(stone, position, fields, move) {
	let placed = place(stone, position);	
	for(let i=0;i<4;i++) {
		fields[placed[i].y][placed[i].x].cell = 0;
	}
	if(!canMove(placed, fields, move)) {
		for(let i=0;i<4;i++) {
			fields[placed[i].y][placed[i].x].cell = 1;
		}
		return false;
	}
	for(let i=0;i<4;i++) {
		fields[placed[i].y+move.y][placed[i].x+move.x].cell = 1;
	}
	return true;
}

function canMove(placed, fields, move) {
	for(let i=0;i<4;i++) {
		let y = placed[i].y+move.y;
		let x = placed[i].x+move.x;

		if(x < borders.x[0] || x > borders.x[1] ||
			y < borders.y[0] || y > borders.y[1]) {
			return false;
		}

		if(fields[y][x].cell > 0) {
			return false;
		}
	}
	return true;
}

class Fields extends React.Component {

	renderSquare({c,r,cell}) {
		return( <Square
			size={19}
			xPos={c*20}
			yPos={r*20}
			color={colorMap[cell]}
		/>);
	}

	render() {
		let squares = [];
		this.props.fields.forEach(
			row => row.forEach(
				field => squares.push(this.renderSquare(field)) 
			)
		);
		return (
		<svg width="300" height="600">
			<g>
				{squares}
			</g>
		</svg>
		);
	}
}

var refreshId;

class Board extends React.Component {
	constructor() {
		super();

		let fields = Array(15).fill(0).map(
			o => Array(10).fill(0));

		// i are rows
		for(let i=0; i<fields.length; i++) {
			// j are columns
			for(let j=0; j<fields[i].length; j++) {
				fields[i][j] = {
					c: j,
					r: i,
					cell: 0 
				};
			}
		}

		this.state = {
			running: false,
			fields: fields,
			position: {x:4, y:0},
			stone: stones[0]
		};

		document.addEventListener("keypress", event => {
			let move;
			const fields = this.state.fields.slice();
			const position = this.state.position;
			if(event.key === "ArrowLeft") {
				move = {x:-1,y:0};
			} else if (event.key === "ArrowRight") {
				move = {x:1,y:0};
			} else {
				return false;
			}

			let moved = step(this.state.stone, position, fields, move);
			if(moved) {
				this.setState({
					fields: fields,
					position: {
						x: position.x+move.x,
						y: position.y+move.y
					}
				});
			}
		});	
	}

	handleClick() {
		if(!this.state.running) {
			let self = this;
			refreshId = setInterval(() => {

				if(self.state.position.y >= self.state.fields.length) {
					clearInterval(refreshId);
					return;
				}

				const fields = self.state.fields.slice();
				let position = self.state.position;

				if(step(self.state.stone, position, fields, {x:0,y:1})) {
					self.setState({
						running: true,
						fields: fields,
						position: {
							x: position.x,
							y: position.y+1
						}
					});
				} else {
					self.setState({
						running: true,
						position: {x:4, y:0}
					});
				}
			}, 300);
		} else {
			clearInterval(refreshId);
			this.setState({
				running: false,
			});
		}	
	}

	render() {
		return (
		<div>
			<Fields fields={this.state.fields} />
			<button onClick={() => this.handleClick()}>
				Action
			</button>
		</div>
		);
	}
}

ReactDOM.render(
	<Board />,
	document.getElementById('root')
);

