import React from 'react';
import ReactDOM from 'react-dom';

class Square extends React.Component {
	render() {
		return (
			<rect
				height={this.props.size}
				width={this.props.size}
				x={this.props.xPos}
				y={this.props.yPos}
				fill={this.props.color}
			/>
		);
	}
}

let borders = {
	x: [0,19],
	y: [0,19],
	w: 20
}

let colorMap = {
	0: "white",
	1: "green",
	2: "red",
}

let stones = [
	[{x:0,y:0},{x:1,y:0},{x:0,y:1},{x:1,y:1}],
	[{x:-1,y:0},{x:0,y:0},{x:1,y:0},{x:2,y:0}],
	[{x:-1,y:0},{x:0,y:0},{x:1,y:0},{x:1,y:1}],
	[{x:-1,y:0},{x:-1,y:1},{x:0,y:1},{x:1,y:1}],
	[{x:0,y:0},{x:-1,y:1},{x:0,y:1},{x:1,y:1}],
	[{x:-1,y:0},{x:0,y:0},{x:0,y:1},{x:1,y:1}],
	[{x:1,y:0},{x:2,y:0},{x:0,y:1},{x:1,y:1}]
]

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getPlaced(stone, offs) {
	let result = [];
	for(let i=0;i<4;i++) {
		result.push({
			x:stone[i].x+offs.x,
			y:stone[i].y+offs.y
		});
	}
	return result;
}

function getRotated(stone) {
	let result = [];
	for(let i=0;i<4;i++) {
		result.push({
			x:-stone[i].y,
			y:stone[i].x
		});
	}
	return result;
}

function canMove(moved, fields) {
	for(let i=0;i<4;i++) {
		let y = moved[i].y;
		let x = moved[i].x;

		if(x < borders.x[0] || x > borders.x[1] ||
			y < borders.y[0] || y > borders.y[1]) {
			return false;
		}

		if(fields[y][x].cell > 0) {
			return false;
		}
	}
	return true;
}

class Fields extends React.Component {

	renderSquare({c,r,cell}) {
		return( 
		<Square
			size={borders.w-1}
			xPos={c*borders.w+4}
			yPos={r*borders.w+4}
			color={colorMap[cell]}
		/>
		);
	}

	render() {
		let squares = [];
		this.props.fields.forEach(
			row => row.forEach(
				field => squares.push(this.renderSquare(field)) 
			)
		);
		return (
		<svg width="500" height="500">
			<rect
				height={borders.w*(borders.y[1]+1)+5}
				width={borders.w*(borders.x[1]+1)+5}
				fill={"none"}
				stroke={"black"}
				x="1"
				y="1"
			/>
			{squares}
		</svg>
		);
	}
}

function getFields() {
	let fields = Array(borders.y[1]+1).fill(0).map(
		o => Array(borders.x[1]+1).fill(0));

	// i are rows
	for(let i=0; i<fields.length; i++) {
		// j are columns
		for(let j=0; j<fields[i].length; j++) {
			fields[i][j] = {
				c: j,
				r: i,
				cell: 0 
			};
		}
	}
	return fields;
}

var refreshId;

const moveLeft = {x:-1,y:0};

const {
	"ArrowLeft": {
		move: moveLeft,
		player: 1
	},
	"ArrowRight": {
		moveRight,
		player: 1
	},
	"ArrowDown": {
		moveDown,
		player: 1
	},
	"ArrowUp": {
		rotate,
		player: 1
	}
};

class Board extends React.Component {
	constructor() {
		super();

		this.state = {
			running: false,
			fields: getFields(),
			position: {x:4, y:0},
			stone: stones[getRandomInt(0,6)]
		};

		document.addEventListener("keypress", event => {
			let move;
			let changed;
			if(event.key === "ArrowLeft") {
				move = {x:-1,y:0};
			} else if (event.key === "ArrowRight") {
				move = {x:1,y:0};
			} else if (event.key === "ArrowDown") {
				move = {x:0,y:1};
			} else if (event.key === "ArrowUp") {
				move = {x:0,y:0};
				changed = getRotated(this.state.stone);	
				if(this.doMove(changed, move)) {
					this.setState({
						stone: changed
					});
				}
			} else {
				return false;
			}
			changed = getPlaced(this.state.stone, move);	
			this.doMove(changed, move);
		});	
	}

	removeLines() {
		const fields = this.state.fields.slice();
		for(let i=1; i<fields.length; i++) {
			const row = fields[i];
			let full = true;
			for(let j=0; j<row.length; j++) {
				if(row[j].cell===0) {
					full = false;
				}
			}
			if(full) {
				for(let k=i; k>0; k--) {
					for(let j=0; j<row.length; j++) {
						fields[k][j].cell = fields[k-1][j].cell;
					}
				}
			}
		}
		return fields;
	}

	setValue(coords, fields, value) {
		for(let i=0;i<coords.length;i++) {
			fields[coords[i].y][coords[i].x].cell = value;
		}
	}

	doMove(moved, offs) {	
		const position = this.state.position;
		const placed = getPlaced(this.state.stone, position);	
		const movedPlaced = getPlaced(moved, position);
		const fields = this.state.fields.slice();

			// reset current cells
			this.setValue(placed, fields, 0);

			if(!canMove(movedPlaced, fields)) {	
				// restore current cells
				this.setValue(placed, fields, 1);
				return false;
			}
			
			// set new cells
			this.setValue(movedPlaced, fields, 1);
			
			this.setState({
				fields: fields,
				position: {
					x: position.x+offs.x,
					y: position.y+offs.y
				}
			});

			return true;
	}

	gameLoop(self) {
		const fields = self.state.fields.slice();
		const position = self.state.position;
		const moved = getPlaced(self.state.stone, {x:0,y:1});

		if(!this.doMove(moved, {x:0,y:1})) {
			// stone did not move
			if(position.y === 0)
			{
				alert("Game over!");
				clearInterval(refreshId);
			} else {
				self.setState({
					fields: this.removeLines(),
					running: true,
					position: {x:4, y:0},
					stone: stones[getRandomInt(0,6)]
				});
			}
		}
	}

	handleClick() {
		if(!this.state.running) {
			refreshId = setInterval(() => this.gameLoop(this), 300);
			this.setState({
				running: true,
			});
		} else {
			clearInterval(refreshId);
			this.setState({
				running: false,
			});
		}	
	}

	render() {
		return (
		<div>
			<Fields fields={this.state.fields} />
			<button onClick={() => this.handleClick()}>
				Action
			</button>
		</div>
		);
	}
}

ReactDOM.render(
	<Board />,
	document.getElementById('root')
);

